package Modelo;

import java.sql.PreparedStatement;
import java.time.LocalDate;

import com.github.lgooddatepicker.components.TimePicker;

public class Turno {

	private Medico medico;
	private Paciente paciente;
	private LocalDate fecha;
	private TimePicker hora;
	private static Turno bd = null;

	public Turno(Medico medico, Paciente paciente, LocalDate fecha, TimePicker hora) {
		super();
		this.medico = medico;
		this.paciente = paciente;
		this.fecha = fecha;
		this.hora = hora;
	}
	
	public Turno() {
		
	}
	
	private static Turno getInstance(){
		if (bd == null) {
			bd = new Turno();
		}
		return bd;

	}
	
	/*
	 * public boolean altaTurno(Turno t) {
	 * 
	 * PreparedStatement s = null; boolean resultado = false; try { // Se crea un
	 * Statement, para realizar la consulta
	 * 
	 * // Se realiza la consulta parametrizada
	 * 
	 * String paciente = t.getPaciente().getNombre(); String apellido =
	 * c.getApellido(); Integer dni = c.getDni(); String email = c.getEmail();
	 * String domicilio = c.getDomicilio(); String localidad = c.getLocalidad();
	 * String telefono = c.getTelefono(); Integer tipoUsuario = c.getTipoUsuario();
	 * 
	 * String consulta =
	 * "insert into Usuario (idUsuario,nombreUsuario,nombre,apellido,contrasenia,dni,email,domicilio,localidad,telefono,tipoUsuario_idtipoUsuario) values (null,?,?,?,?,?,?,?,?,?,?,0)"
	 * ;
	 * 
	 * s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);
	 * 
	 * s.setString(1, nombreUsuario); s.setString(2, nombre); s.setString(3,
	 * apellido); s.setString(4, contrasenia); s.setInt(5, dni); s.setString(6,
	 * email); s.setString(7, domicilio); s.setString(8, localidad); s.setString(9,
	 * telefono); s.setInt(10, tipoUsuario);
	 * 
	 * s.execute(); resultado = true;
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } return resultado; }
	 */
	
	

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public TimePicker getHora() {
		return hora;
	}

	public void setHora(TimePicker hora) {
		this.hora = hora;
	}
	
	

}
