package Modelo;

import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Date;

public class Paciente {

	private String nombre;
	private String apellido;
	private Integer dni;
	private String domicilio;
	private String localidad;
	private LocalDate fechaNacimiento;
	private String obraSocial;
	private String sexo;
	private String email;
	private String telefono;
	private static Paciente bd = null;

	public Paciente(String nombre, String apellido, Integer dni, String domicilio, String localidad,
			LocalDate fechaNacimiento, String obraSocial, String sexo, String email, String telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.domicilio = domicilio;
		this.localidad = localidad;
		this.fechaNacimiento = fechaNacimiento;
		this.obraSocial = obraSocial;
		this.sexo = sexo;
		this.email = email;
		this.telefono = telefono;
	}

	public Paciente() {

	}

	public static Paciente getInstance() {

		if (bd == null) {
			bd = new Paciente();
		}
		return bd;

	}
	
	//ALTA PACIENTE
	
	public boolean altaPaciente(Paciente p) {

		PreparedStatement s = null;
		boolean resultado = false;
		try {
			// Se crea un Statement, para realizar la consulta

			// Se realiza la consulta parametrizada

			String nombre = p.getNombre();
			String apellido = p.getApellido();
			Integer dni = p.getDni();
			String domicilio = p.getDomicilio();
			String localidad = p.getLocalidad();
			LocalDate fechaNacimiento = p.getFechaNacimiento();
			String obraSocial = p.getObraSocial();
			String sexo = p.getSexo();
			String email = p.getEmail();
			String telefono = p.getTelefono();
			Integer baja = 0;

			String consulta = "insert into paciente (idpaciente,nombre,apellido,dni,domicilio,localidad,fechaNacimiento,obraSocial,sexo,email,telefono,baja) values (null,?,?,?,?,?,?,?,?,?,?,?)";

			s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);

			s.setString(1, nombre);
			s.setString(2, apellido);
			s.setInt(3, dni);
			s.setString(4, domicilio);
			s.setString(5, localidad);
			s.setString(6, String.valueOf(fechaNacimiento));
			s.setString(7, obraSocial);
			s.setString(8, sexo);
			s.setString(9, email);
			s.setString(10, telefono);
			s.setInt(11, baja);

			s.execute();
			resultado = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

	public String getObraSocial() {
		return obraSocial;
	}

	public void setObraSocial(String obraSocial) {
		this.obraSocial = obraSocial;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "Paciente [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", domicilio=" + domicilio
				+ ", localidad=" + localidad + ", fechaNacimiento=" + fechaNacimiento + ", obraSocial=" + obraSocial
				+ ", sexo=" + sexo + ", email=" + email + ", telefono=" + telefono + "]";
	}

}
