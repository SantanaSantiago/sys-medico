package Vista;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador5_ABM_medicos;

public class Vista5_ABM_medicos extends JPanel {

	private JTextField txtBusqueda;
	private JButton btnBuscar;
	private JButton btnCargar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JTable tablaTurnos;
	private JPanel panelABM;
	private Controlador5_ABM_medicos controladorMedicos;
	
	
	public Vista5_ABM_medicos(Controlador5_ABM_medicos controladorMedicos) {
		this.setControladorMedicos(controladorMedicos);
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 60, 445, 380);
		add(scrollPane);

		tablaTurnos = new JTable();
		tablaTurnos.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Dni", "nombre", "apellido", "Especialidad" }));
		scrollPane.setViewportView(tablaTurnos);

		txtBusqueda = new JTextField();
		txtBusqueda.addActionListener(getControladorMedicos());
		txtBusqueda.setBounds(139, 4, 201, 42);
		add(txtBusqueda);
		txtBusqueda.setColumns(10);

		panelABM = new JPanel();
		panelABM.setBounds(455, 285, 103, 155);
		add(panelABM);
				panelABM.setLayout(new GridLayout(0, 1, 0, 0));
		
				btnCargar = new JButton("Cargar");
				panelABM.add(btnCargar);
				
						btnEliminar = new JButton("Eliminar");
						panelABM.add(btnEliminar);
						
								btnModificar = new JButton("Modificar");
								panelABM.add(btnModificar);
								
								btnBuscar = new JButton("Buscar");
								btnBuscar.setBounds(6, 6, 117, 42);
								add(btnBuscar);
								btnModificar.addActionListener(getControladorMedicos());
						btnEliminar.addActionListener(getControladorMedicos());

		btnCargar.addActionListener(getControladorMedicos());
	

	}


	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}


	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}


	public JButton getBtnCargar() {
		return btnCargar;
	}


	public void setBtnCargar(JButton btnCargar) {
		this.btnCargar = btnCargar;
	}


	public JButton getBtnModificar() {
		return btnModificar;
	}


	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}


	public JButton getBtnEliminar() {
		return btnEliminar;
	}


	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}


	public JTable getTablaTurnos() {
		return tablaTurnos;
	}


	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}





	public Controlador5_ABM_medicos getControladorMedicos() {
		return controladorMedicos;
	}


	public void setControladorMedicos(Controlador5_ABM_medicos controladorMedicos) {
		this.controladorMedicos = controladorMedicos;
	}


	public JButton getBtnBuscar() {
		return btnBuscar;
	}


	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}


	public JPanel getPanelABM() {
		return panelABM;
	}


	public void setPanelABM(JPanel panelABM) {
		this.panelABM = panelABM;
	}

}
