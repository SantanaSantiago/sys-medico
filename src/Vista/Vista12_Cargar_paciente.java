package Vista;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import Controlador.Controlador12_Cargar_Paciente;

public class Vista12_Cargar_paciente extends JPanel {

	private Controlador12_Cargar_Paciente controladorCP;
	private JComboBox comboSexo;
	private JComboBox comboObraSocial;
	private JTextField txtApellido;
	private JTextField txtNombre;
	private JTextField txtDNI;
	private JTextField txtDomicilio;
	private JTextField txtLocalidad;
	private JFormattedTextField txtFechaNac;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JButton btnAceptar;
	private JButton btnCancelar;

	public Vista12_Cargar_paciente(Controlador12_Cargar_Paciente controladorCP) {
		this.setControladorCP(controladorCP);

		setLayout(new GridLayout(0, 4, 0, 0));

		JLabel lblNuevoUsuario = new JLabel("      NUEVO PACIENTE");
		add(lblNuevoUsuario);

		JLabel label = new JLabel("");
		add(label);

		JLabel label_1 = new JLabel("");
		add(label_1);

		JLabel label_2 = new JLabel("");
		add(label_2);

		JLabel label_3 = new JLabel("");
		add(label_3);

		JLabel label_4 = new JLabel("");
		add(label_4);

		JLabel label_5 = new JLabel("");
		add(label_5);

		JLabel label_6 = new JLabel("");
		add(label_6);

		JLabel lblObraSocial = new JLabel("      Obra Social");
		add(lblObraSocial);

		comboObraSocial = new JComboBox();
		comboObraSocial.setModel(new DefaultComboBoxModel(new String[] { "OSDE", "OSDIPP", "ACA Salud", "SANCOR" }));
		add(comboObraSocial);

		JLabel lblfechaNac = new JLabel("      Fecha Nacimiento");
		add(lblfechaNac);

		DateFormat df = new SimpleDateFormat("dd/mm/aaaa");
		txtFechaNac = new JFormattedTextField(df);
		txtFechaNac.setFocusLostBehavior(JFormattedTextField.PERSIST);
		add(txtFechaNac);

		try {

			MaskFormatter dateMask = new MaskFormatter("##/##/####");
			dateMask.install(txtFechaNac);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		JLabel lblApellido = new JLabel("      Apellido");
		add(lblApellido);

		txtApellido = new JTextField("");
		add(txtApellido);

		JLabel lblTelefono = new JLabel("      Telefono");
		add(lblTelefono);

		txtTelefono = new JTextField("");
		add(txtTelefono);

		JLabel lblNombre = new JLabel("      Nombre");
		add(lblNombre);

		txtNombre = new JTextField("");
		add(txtNombre);

		JLabel lblSexo = new JLabel("      Sexo");
		add(lblSexo);

		comboSexo = new JComboBox();
		comboSexo.setModel(new DefaultComboBoxModel(new String[] { "Mujer", "Varon" }));
		add(comboSexo);

		JLabel lblDni = new JLabel("      DNI");
		add(lblDni);

		txtDNI = new JTextField("");
		add(txtDNI);

		JLabel lblEmail = new JLabel("      Email");
		add(lblEmail);

		txtEmail = new JTextField("");
		add(txtEmail);

		JLabel lblDomicilio = new JLabel("      Domicilio");
		add(lblDomicilio);

		txtDomicilio = new JTextField("");
		add(txtDomicilio);

		JLabel label_16 = new JLabel("");
		add(label_16);

		JLabel label_21 = new JLabel("");
		add(label_21);

		JLabel lblLocalidad = new JLabel("      Localidad");
		add(lblLocalidad);

		txtLocalidad = new JTextField("");
		add(txtLocalidad);

		JLabel label_23 = new JLabel("");
		add(label_23);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControladorCP());
		add(btnAceptar);

		JLabel label_24 = new JLabel("");
		add(label_24);

		JLabel label_25 = new JLabel("");
		add(label_25);

		JLabel label_26 = new JLabel("");
		add(label_26);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(getControladorCP());
		add(btnCancelar);

	}

	public JComboBox getComboTipoUsuario() {
		return comboSexo;
	}

	public void setComboTipoUsuario(JComboBox comboTipoUsuario) {
		this.comboSexo = comboTipoUsuario;
	}

	public JComboBox getComboObraSocial() {
		return comboObraSocial;
	}

	public void setTxtnombreUsuario(JComboBox comboObraSocial) {
		this.comboObraSocial = comboObraSocial;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}


	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public void setTxtDNI(JTextField txtDNI) {
		this.txtDNI = txtDNI;
	}

	public JTextField getTxtDomicilio() {
		return txtDomicilio;
	}

	public void setTxtDomicilio(JTextField txtDomicilio) {
		this.txtDomicilio = txtDomicilio;
	}

	public JTextField getTxtLocalidad() {
		return txtLocalidad;
	}

	public void setTxtLocalidad(JTextField txtLocalidad) {
		this.txtLocalidad = txtLocalidad;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public Controlador12_Cargar_Paciente getControladorCP() {
		return controladorCP;
	}

	public void setControladorCP(Controlador12_Cargar_Paciente controladorCP) {
		this.controladorCP = controladorCP;
	}

	public JComboBox getComboSexo() {
		return comboSexo;
	}

	public void setComboSexo(JComboBox comboSexo) {
		this.comboSexo = comboSexo;
	}

	public void setComboObraSocial(JComboBox comboObraSocial) {
		this.comboObraSocial = comboObraSocial;
	}

	public JFormattedTextField getTxtFechaNac() {
		return txtFechaNac;
	}

	public void setTxtFechaNac(JFormattedTextField txtFechaNac) {
		this.txtFechaNac = txtFechaNac;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

}
