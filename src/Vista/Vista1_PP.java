package Vista;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;

import Controlador.Controlador1_PP;
import Controlador.Controlador3_ABM_turnos;
import Controlador.Controlador4_ABM_usuarios;
import Controlador.Controlador5_ABM_medicos;
import Controlador.Controlador7_ABM_pacientes;
import Controlador.Controlador8_reportes;
import Controlador.Controlador10_Cargar_usuario;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import javax.swing.UIManager;
import java.awt.FlowLayout;

public class Vista1_PP extends JFrame {

	private JPanel contentPane;
	private Controlador1_PP controlador;
	private JPanel panel_Turnos;
	private JPanel panel_Usuarios;
	private JPanel panel_Reportes;
	private JPanel panel_Gestion;
	private JPanel panelMedicos;
	private JPanel panel_Card;
	private JPanel panelBotonera;
	private JButton btnUsuarios_1;
	private JButton btnTurnos_1;
	private JButton btnReportes;
	private JButton btnMedicos;
	private JPanel panelTurnos;
	private JPanel panelPacientes;
	private JPanel panelUsuarios;
	private JPanel panelCargarTurno;
	private JPanel panelCargarPaciente;
	private JPanel panelModificarUsuario;
	private JButton btnPacientes;
	private JPanel panelLogos;
	private JLabel lblimagen;
	private JPanel panelCargarUsuario;

	
	public Vista1_PP(Controlador1_PP controlador) {
		this.setControlador(controlador);
		this.setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 764, 482);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		contentPane.setLayout(null);

		panel_Card = new JPanel();
		panel_Card.setBackground(Color.CYAN);
		panel_Card.setBounds(216, 6, 559, 477);
		contentPane.add(panel_Card);
		panel_Card.setLayout(new CardLayout(0, 0));

		panelTurnos = new JPanel();
		panelTurnos.setName("TURNOS");
		panelTurnos = this.getControlador().crearPanel(panelTurnos.getName());
		panel_Card.add(panelTurnos, "TURNOS");

		panelUsuarios = new JPanel();
		panelUsuarios.setName("USUARIOS");
		panelUsuarios = this.getControlador().crearPanel(panelUsuarios.getName());
		panel_Card.add(panelUsuarios, "USUARIOS");

		panel_Reportes = new JPanel();
		panel_Reportes.setName("REPORTES");
		panel_Reportes = this.getControlador().crearPanel(panel_Reportes.getName());
		panel_Card.add(panel_Reportes, "REPORTES");

		panelMedicos = new JPanel();
		panelMedicos.setName("MEDICOS");
		panelMedicos = this.getControlador().crearPanel(panelMedicos.getName());
		panel_Card.add(panelMedicos, "MEDICOS");

		panel_Gestion = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_Gestion.getLayout();
		flowLayout.setAlignOnBaseline(true);
		panel_Card.add(panel_Gestion, "GESTION");

		panelPacientes = new JPanel();
		panelPacientes.setName("PACIENTES");
		panelPacientes = this.getControlador().crearPanel(panelPacientes.getName());
		panel_Card.add(panelPacientes, "PACIENTES");

		panelCargarUsuario = new JPanel();
		panelCargarUsuario.setName("CARGAR_US");
		panelCargarUsuario = this.getControlador().crearPanel(panelCargarUsuario.getName());
		panel_Card.add(panelCargarUsuario, "CARGAR_US");

		// Crear el atributo JPanel --- panelCargarTurno
		// Crar un nuevo panel --- panelCargarTurno = new JPanel();
		panelCargarTurno = new JPanel();
		panelCargarTurno.setName("CARGAR_TU");
		panelCargarTurno = this.getControlador().crearPanel(panelCargarTurno.getName());
		panel_Card.add(panelCargarTurno, "CARGAR_TU");
		
		panelCargarPaciente = new JPanel();
		panelCargarPaciente.setName("CARGAR_PA");
		panelCargarPaciente = this.getControlador().crearPanel(panelCargarPaciente.getName());
		panel_Card.add(panelCargarPaciente, "CARGAR_PA");
		
		panelModificarUsuario = new JPanel();
		panelModificarUsuario.setName("MODIFICAR_U");
		panelModificarUsuario = this.getControlador().crearPanel(panelModificarUsuario.getName());
		panel_Card.add(panelModificarUsuario, "MODIFICAR_U");
		
		
		
		panelBotonera = new JPanel();
		panelBotonera.setBackground(UIManager.getColor("ComboBox.background"));
		panelBotonera.setBounds(6, 183, 209, 262);
		contentPane.add(panelBotonera);
		panelBotonera.setLayout(new GridLayout(0, 1, 0, 0));

		
		
		btnTurnos_1 = new JButton("TURNOS");
		btnTurnos_1.addActionListener(getControlador());
		panelBotonera.add(btnTurnos_1);

		btnPacientes = new JButton("PACIENTES");
		btnPacientes.addActionListener(getControlador());
		panelBotonera.add(btnPacientes);

		btnMedicos = new JButton("MEDICOS");
		btnMedicos.addActionListener(getControlador());
		panelBotonera.add(btnMedicos);

		btnReportes = new JButton("REPORTES");
		btnReportes.addActionListener(getControlador());

		btnUsuarios_1 = new JButton("USUARIOS");
		btnUsuarios_1.addActionListener(getControlador());
		panelBotonera.add(btnUsuarios_1);
		panelBotonera.add(btnReportes);

		panelLogos = new JPanel();
		panelLogos.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		panelLogos.setToolTipText("Imagen");
		panelLogos.setBackground(SystemColor.window);
		panelLogos.setBounds(6, 6, 209, 173);
		contentPane.add(panelLogos);
		panelLogos.setLayout(null);

		lblimagen = new JLabel("Imagen");

		lblimagen.setBounds(81, 73, 61, 16);
		panelLogos.add(lblimagen);
	}

	public JPanel getPanel_Card() {
		return panel_Card;
	}

	public void setPanel_Card(JPanel panel_Card) {
		this.panel_Card = panel_Card;
	}

	public JPanel getPanelTabla() {
		return panelMedicos;
	}

	public void setPanelTabla(JPanel panelTabla) {
		this.panelMedicos = panelTabla;
	}

	public JPanel getPanel_Turnos() {
		return panel_Turnos;
	}

	public void setPanel_Turnos(JPanel panel_Turnos) {
		this.panel_Turnos = panel_Turnos;
	}

	public JPanel getPanel_Usuarios() {
		return panel_Usuarios;
	}

	public void setPanel_Usuarios(JPanel panel_Usuarios) {
		this.panel_Usuarios = panel_Usuarios;
	}

	public JPanel getPanel_Reportes() {
		return panel_Reportes;
	}

	public void setPanel_Reportes(JPanel panel_Reportes) {
		this.panel_Reportes = panel_Reportes;
	}

	public JPanel getPanel_Gestion() {
		return panel_Gestion;
	}

	public void setPanel_Gestion(JPanel panel_Gestion) {
		this.panel_Gestion = panel_Gestion;
	}

	public JButton getBtnReportes() {
		return btnReportes;
	}

	public void setBtnReportes(JButton btnReportes) {
		this.btnReportes = btnReportes;
	}

	public Controlador1_PP getControlador() {
		return controlador;
	}

	public void setControlador(Controlador1_PP controlador) {
		this.controlador = controlador;
	}

	public JPanel getPanelBotonera() {
		return panelBotonera;
	}

	public void setPanelBotonera(JPanel panelBotonera) {
		this.panelBotonera = panelBotonera;
	}

	public JButton getBtnUsuarios_1() {
		return btnUsuarios_1;
	}

	public void setBtnUsuarios_1(JButton btnUsuarios_1) {
		this.btnUsuarios_1 = btnUsuarios_1;
	}

	public JButton getBtnTurnos_1() {
		return btnTurnos_1;
	}

	public void setBtnTurnos_1(JButton btnTurnos_1) {
		this.btnTurnos_1 = btnTurnos_1;
	}

	public JButton getBtnMedicos() {
		return btnMedicos;
	}

	public void setBtnMedicos(JButton btnMedicos) {
		this.btnMedicos = btnMedicos;
	}

	public JPanel getPanelTurnos() {
		return panelTurnos;
	}

	public void setPanelTurnos(JPanel panelTurnos) {
		this.panelTurnos = panelTurnos;
	}

	public JPanel getPanelPacientes() {
		return panelPacientes;
	}

	public void setPanelPacientes(JPanel panelPacientes) {
		this.panelPacientes = panelPacientes;
	}

	public JPanel getPanelUsuarios() {
		return panelUsuarios;
	}

	public void setPanelUsuarios(JPanel panelUsuarios) {
		this.panelUsuarios = panelUsuarios;
	}

	public JPanel getPanelMedicos() {
		return panelMedicos;
	}

	public void setPanelMedicos(JPanel panelMedicos) {
		this.panelMedicos = panelMedicos;
	}

	public JButton getBtnPacientes() {
		return btnPacientes;
	}

	public void setBtnPacientes(JButton btnPacientes) {
		this.btnPacientes = btnPacientes;
	}

	public JPanel getPanelLogos() {
		return panelLogos;
	}

	public void setPanelLogos(JPanel panelLogos) {
		this.panelLogos = panelLogos;
	}

	public JLabel getLblimagen() {
		return lblimagen;
	}

	public void setLblimagen(JLabel lblimagen) {
		this.lblimagen = lblimagen;
	}

	public JPanel getPanelCargarUsuario() {
		return panelCargarUsuario;
	}

	public void setPanelCargarUsuario(JPanel panelCargarUsuario) {
		this.panelCargarUsuario = panelCargarUsuario;
	}

	public JPanel getPanelCargarTurno() {
		return panelCargarTurno;
	}

	public void setPanelCargarTurno(JPanel panelCargarTurno) {
		this.panelCargarTurno = panelCargarTurno;
	}

	public JPanel getPanelCargarPaciente() {
		return panelCargarPaciente;
	}

	public void setPanelCargarPaciente(JPanel panelCargarPaciente) {
		this.panelCargarPaciente = panelCargarPaciente;
	}

	public JPanel getPanelModificarUsuario() {
		return panelModificarUsuario;
	}

	public void setPanelModificarUsuario(JPanel panelModificarUsuario) {
		this.panelModificarUsuario = panelModificarUsuario;
	}

}
