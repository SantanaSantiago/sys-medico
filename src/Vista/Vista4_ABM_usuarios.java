package Vista;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador3_ABM_turnos;
import Controlador.Controlador4_ABM_usuarios;

public class Vista4_ABM_usuarios extends JPanel {

	private JTextField txtBusqueda;
	private JButton btnCargar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JTable tablaUsuarios;
	private JPanel panelABM;
	private JButton btnBuscar;
	private Controlador4_ABM_usuarios controladorUsuarios;

	public Vista4_ABM_usuarios(Controlador4_ABM_usuarios controladorUsuarios) {
		this.setControladorUsuarios(controladorUsuarios);
		System.out.println("Estoy en la vista Usuarios.. ");
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 60, 445, 380);
		add(scrollPane);

		tablaUsuarios = new JTable();
		tablaUsuarios.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "nombre", "apellido", "dni", "Tipo de Usuario" }));
		scrollPane.setViewportView(tablaUsuarios);

		txtBusqueda = new JTextField();
		txtBusqueda.addActionListener(getControladorUsuarios());
		txtBusqueda.setBounds(139, 4, 201, 42);
		add(txtBusqueda);
		txtBusqueda.setColumns(10);

		panelABM = new JPanel();
		panelABM.setBounds(455, 285, 103, 155);
		add(panelABM);
		panelABM.setLayout(new GridLayout(0, 1, 0, 0));

		btnCargar = new JButton("Cargar");
		btnCargar.addActionListener(getControladorUsuarios());
		panelABM.add(btnCargar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(getControladorUsuarios());
		panelABM.add(btnEliminar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(getControladorUsuarios());
		panelABM.add(btnModificar);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(6, 6, 117, 42);
		add(btnBuscar);

	}

	public Controlador4_ABM_usuarios getControladorUsuarios() {
		return controladorUsuarios;
	}

	public void setControladorUsuarios(Controlador4_ABM_usuarios controladorUsuarios) {
		this.controladorUsuarios = controladorUsuarios;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}

	public JButton getBtnCargar() {
		return btnCargar;
	}

	public void setBtnCargar(JButton btnCargar) {
		this.btnCargar = btnCargar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JTable getTablaTurnos() {
		return tablaUsuarios;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaUsuarios = tablaTurnos;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public JTable getTablaUsuarios() {
		return tablaUsuarios;
	}

	public void setTablaUsuarios(JTable tablaUsuarios) {
		this.tablaUsuarios = tablaUsuarios;
	}

	public JPanel getPanelABM() {
		return panelABM;
	}

	public void setPanelABM(JPanel panelABM) {
		this.panelABM = panelABM;
	}

}
