package Vista;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Controlador.Controlador2_Login;

import javax.swing.JButton;

public class Vista2_Login extends JPanel {
	
	private JTextField txtNombreUsuario;
	private JTextField txtPassword;
	private JButton btnAceptar;
	
	private Controlador2_Login controlador;
	

	public Vista2_Login(Controlador2_Login controlador) {
		
		this.setControlador(controlador);
		setLayout(null);
		
		JLabel lblNombreDeUsuario = new JLabel("Nombre de Usuario");
		lblNombreDeUsuario.setBounds(65, 150, 121, 16);
		add(lblNombreDeUsuario);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(65, 196, 61, 16);
		add(lblPassword);
		
		txtNombreUsuario = new JTextField();
		txtNombreUsuario.setBounds(198, 145, 182, 26);
		add(txtNombreUsuario);
		txtNombreUsuario.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(198, 191, 182, 26);
		add(txtPassword);
		txtPassword.setColumns(10);
		
		JButton btnImagen = new JButton("Imagen");
		btnImagen.setBounds(142, 25, 156, 98);
		add(btnImagen);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControlador());
		btnAceptar.setBounds(165, 239, 117, 29);
		add(btnAceptar);

	}


	public JTextField getTxtNombreUsuario() {
		return txtNombreUsuario;
	}


	public void setTxtNombreUsuario(JTextField txtNombreUsuario) {
		this.txtNombreUsuario = txtNombreUsuario;
	}


	public JTextField getTxtPassword() {
		return txtPassword;
	}


	public void setTxtPassword(JTextField txtPassword) {
		this.txtPassword = txtPassword;
	}


	public Controlador2_Login getControlador() {
		return controlador;
	}


	public void setControlador(Controlador2_Login controlador) {
		this.controlador = controlador;
	}


	public JButton getBtnAceptar() {
		return btnAceptar;
	}


	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}
}
