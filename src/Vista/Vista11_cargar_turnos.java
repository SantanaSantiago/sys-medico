package Vista;

import javax.swing.JPanel;

import Controlador.Controlador11_cargar_turnos;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import com.toedter.calendar.JDateChooser;
import javax.swing.JTextField;
import javax.swing.JButton;
import com.github.lgooddatepicker.components.TimePicker;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

public class Vista11_cargar_turnos extends JPanel {
    private Controlador11_cargar_turnos controlador;
    private JTable table;
    private JScrollPane scrollPane_1;
    private JTable table_2;
    private JLabel lblPaciente;
    private JLabel lblDoctor;
    private JLabel lblSelecioneLaFecha;
    private JLabel lblSelecioneLaHora;
    private JButton btnCancelar;
    private TimePicker timepicker;
    private JButton btnAceptar;
	

	public Vista11_cargar_turnos(Controlador11_cargar_turnos controlador) {
		setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.setControlador(controlador);
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 28, 346, 170);
		add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"D.N.I", "Nombre", "Apellido", "Obra Social"
			}
		));
		scrollPane.setViewportView(table);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(44, 221, 346, 165);
		add(scrollPane_1);
		
		table_2 = new JTable();
		table_2.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"D.N.I", "Nombre", "Especialidad"
			}
		));
		scrollPane_1.setViewportView(table_2);
		
		lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(44, 6, 52, 16);
		add(lblPaciente);
		
		lblDoctor = new JLabel("Doctor");
		lblDoctor.setBounds(44, 196, 43, 29);
		add(lblDoctor);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(44, 426, 107, 29);
		add(dateChooser);
		
		lblSelecioneLaFecha = new JLabel("Selecione la fecha");
		lblSelecioneLaFecha.setBounds(44, 398, 136, 14);
		add(lblSelecioneLaFecha);
		
		lblSelecioneLaHora = new JLabel("Selecione la hora");
		lblSelecioneLaHora.setBounds(263, 397, 106, 16);
		add(lblSelecioneLaHora);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(402, 271, 141, 57);
		add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(402, 329, 141, 57);
		add(btnCancelar);
		
		TimePicker timePicker = new TimePicker();
		timePicker.getComponentTimeTextField().setForeground(Color.WHITE);
		timePicker.getComponentTimeTextField().setBorder(UIManager.getBorder("TextField.border"));
		timePicker.setBounds(263, 426, 127, 29);
		add(timePicker);

	}




	public JTable getTable() {
		return table;
	}



	public void setTable(JTable table) {
		this.table = table;
	}




	public JScrollPane getScrollPane_1() {
		return scrollPane_1;
	}





	public void setScrollPane_1(JScrollPane scrollPane_1) {
		this.scrollPane_1 = scrollPane_1;
	}



	public JTable getTable_2() {
		return table_2;
	}







	public void setTable_2(JTable table_2) {
		this.table_2 = table_2;
	}






	public JLabel getLblPaciente() {
		return lblPaciente;
	}




	public void setLblPaciente(JLabel lblPaciente) {
		this.lblPaciente = lblPaciente;
	}







	public JLabel getLblDoctor() {
		return lblDoctor;
	}







	public void setLblDoctor(JLabel lblDoctor) {
		this.lblDoctor = lblDoctor;
	}





	public JLabel getLblSelecioneLaFecha() {
		return lblSelecioneLaFecha;
	}





	public void setLblSelecioneLaFecha(JLabel lblSelecioneLaFecha) {
		this.lblSelecioneLaFecha = lblSelecioneLaFecha;
	}




	public JLabel getLblSelecioneLaHora() {
		return lblSelecioneLaHora;
	}




	public void setLblSelecioneLaHora(JLabel lblSelecioneLaHora) {
		this.lblSelecioneLaHora = lblSelecioneLaHora;
	}


	


	public Controlador11_cargar_turnos getControlador() {
		return controlador;
	}


	public void setControlador(Controlador11_cargar_turnos controlador) {
		this.controlador = controlador;
	}




	public JButton getBtnCancelar() {
		return btnCancelar;
	}




	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}




	public TimePicker getTimepicker() {
		return timepicker;
	}




	public void setTimepicker(TimePicker timepicker) {
		this.timepicker = timepicker;
	}




	public JButton getBtnAceptar() {
		return btnAceptar;
	}




	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}
}
