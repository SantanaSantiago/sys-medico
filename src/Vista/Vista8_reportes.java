package Vista;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Controlador.Controlador4_ABM_usuarios;
import Controlador.Controlador8_reportes;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.GridLayout;

import com.toedter.calendar.JDateChooser;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Vista8_reportes extends JPanel {

	private Controlador8_reportes controladorReportes;
	private JTextField txtBusqueda;
	private JButton btnReporte;
	private JButton btnPlantilla;
	private JTable tablaTurnos;
	private JPanel panelABM;
	private JButton btnBuscar;

	public Vista8_reportes(Controlador8_reportes controladorReportes) {

		this.setControladorReportes(controladorReportes);

		System.out.println("Estoy en la vista Usuarios.. ");
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 60, 445, 380);
		add(scrollPane);

		tablaTurnos = new JTable();
		tablaTurnos.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Dni", "nombre", "apellido", "Especialidad" }));
		scrollPane.setViewportView(tablaTurnos);

		txtBusqueda = new JTextField();
		txtBusqueda.addActionListener(getControladorReportes());
		txtBusqueda.setBounds(139, 4, 201, 42);
		add(txtBusqueda);
		txtBusqueda.setColumns(10);

		panelABM = new JPanel();
		panelABM.setBounds(455, 292, 103, 148);
		add(panelABM);
		panelABM.setLayout(new GridLayout(0, 1, 0, 0));

		btnReporte = new JButton("Reporte");
		btnReporte.addActionListener(getControladorReportes());
		panelABM.add(btnReporte);

		btnPlantilla = new JButton("Plantilla");
		btnPlantilla.addActionListener(getControladorReportes());
		
		JButton btnTurnosDiarios = new JButton("Turnos diarios");
		panelABM.add(btnTurnosDiarios);
		panelABM.add(btnPlantilla);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(6, 6, 117, 42);
		add(btnBuscar);
		
		
		JDateChooser fechaDesde = new JDateChooser();
		fechaDesde.setBounds(455, 185, 103, 26);
		add(fechaDesde);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(455, 238, 103, 26);
		add(dateChooser);
		
		JLabel lblFechaDesde = new JLabel("Fecha desde");
		lblFechaDesde.setBounds(455, 162, 95, 16);
		add(lblFechaDesde);
		
		JLabel lblFechaHasta = new JLabel("Fecha hasta");
		lblFechaHasta.setBounds(455, 216, 95, 16);
		add(lblFechaHasta);
        
	}



	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}

	public JButton getBtnCargar() {
		return btnReporte;
	}

	public void setBtnCargar(JButton btnCargar) {
		this.btnReporte = btnCargar;
	}

	public JButton getBtnModificar() {
		return btnPlantilla;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnPlantilla = btnModificar;
	}



	public JTable getTablaTurnos() {
		return tablaTurnos;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public Controlador8_reportes getControladorReportes() {
		return controladorReportes;
	}

	public void setControladorReportes(Controlador8_reportes contraladorReportes) {
		this.controladorReportes = contraladorReportes;
		setLayout(null);
	}
}
