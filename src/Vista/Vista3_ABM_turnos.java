package Vista;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador3_ABM_turnos;

import javax.swing.JLabel;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.GridLayout;

public class Vista3_ABM_turnos extends JPanel {
	private JTextField txtBusqueda;
	private JButton btnCargar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JPanel panelABM;
	private JButton btnBuscar;
	private JTable tablaTurnos;
	private Controlador3_ABM_turnos controlador;





	public Vista3_ABM_turnos(Controlador3_ABM_turnos controlador) {
		this.setControlador(controlador);
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 60, 445, 380);
		add(scrollPane);

		tablaTurnos = new JTable();
		tablaTurnos.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Dni", "nombre", "apellido", "Fecha turno", "Hora turno", "Medico", "especialidad" }));
		scrollPane.setViewportView(tablaTurnos);

		txtBusqueda = new JTextField();
		txtBusqueda.addActionListener(getControlador());
		txtBusqueda.setBounds(139, 4, 201, 42);
		add(txtBusqueda);
		txtBusqueda.setColumns(10);

		

		panelABM = new JPanel();
		panelABM.setBounds(455, 285, 103, 155);
		add(panelABM);
				panelABM.setLayout(new GridLayout(0, 1, 0, 0));
		
				btnCargar = new JButton("Cargar");
				panelABM.add(btnCargar);
				
						btnEliminar = new JButton("Eliminar");
						panelABM.add(btnEliminar);
						
								btnModificar = new JButton("Modificar");
								panelABM.add(btnModificar);
								
								btnBuscar = new JButton("Buscar");
								btnBuscar.setBounds(6, 6, 117, 42);
								add(btnBuscar);
								btnModificar.addActionListener(getControlador());
						btnEliminar.addActionListener(getControlador());

		btnCargar.addActionListener(getControlador());
	

	}
	

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}

	public JButton getBtnCargar() {
		return btnCargar;
	}

	public void setBtnCargar(JButton btnCargar) {
		this.btnCargar = btnCargar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JTable getTablaTurnos() {
		return tablaTurnos;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}

	public Controlador3_ABM_turnos getControlador() {
		return controlador;
	}

	public void setControlador(Controlador3_ABM_turnos controlador) {
		this.controlador = controlador;
	}


	public JPanel getPanelABM() {
		return panelABM;
	}


	public void setPanelABM(JPanel panelABM) {
		this.panelABM = panelABM;
	}


	public JButton getBtnBuscar() {
		return btnBuscar;
	}


	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}
}
