package Controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;

import Modelo.Usuario;
import Vista.Vista13_Modificar_usuario;

public class Controlador13_Modificar_usuario implements ActionListener, WindowListener {

	private Vista13_Modificar_usuario vistaMU;
	private Integer dni;
	private String nombreUsuario;
	private String contrasenia;
	private String nombre;
	private String apellido;
	private String email;
	private String telefono;
	private String domicilio;
	private String localidad;
	private Integer tipoUsuario;
	private Usuario usuario;
	private Controlador1_PP controladorPP;

	public Controlador13_Modificar_usuario(Usuario usuario, Controlador1_PP controladorPP) {

		this.setVistaMU(new Vista13_Modificar_usuario(this));
		this.setUsuario(usuario);
		//this.windowActivated(null);
		this.setControladorPP(controladorPP);
		
		
		CardLayout cl = (CardLayout) this.getControladorPP().getVistaPrincipal().getPanel_Card().getLayout();
	      cl.show(this.getControladorPP().getVistaPrincipal().getPanel_Card(),"MODIFICAR_U");

	}

	public Controlador13_Modificar_usuario(Integer dni, String nombreUsuario, String contrasenia, String nombre,
			String apellido, String email, String telefono, String domicilio, String localidad, Integer tipoUsuario) {
		super();

		this.setVistaMU(new Vista13_Modificar_usuario(this));
		this.setUsuario(usuario);
		this.dni = dni;
		this.nombreUsuario = nombreUsuario;
		this.contrasenia = contrasenia;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.domicilio = domicilio;
		this.localidad = localidad;
		this.tipoUsuario = tipoUsuario;
		this.usuario = new Usuario();
	}

	public Controlador13_Modificar_usuario() {
		this.setVistaMU(new Vista13_Modificar_usuario(this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVistaMU().getBtnAceptar()) {

			if (this.getVistaMU().getTxtDNI().getText().isEmpty()
					|| this.getVistaMU().getTxtnombreUsuario().getText().isEmpty()
					|| this.getVistaMU().getTxtContrasenia().getText().isEmpty()
					|| this.getVistaMU().getTxtNombre().getText().isEmpty()
					|| this.getVistaMU().getTxtApellido().getText().isEmpty()
					|| this.getVistaMU().getTxtEmail().getText().isEmpty()
					|| this.getVistaMU().getTxtTelefono().getText().isEmpty()
					|| this.getVistaMU().getTxtDomicilio().getText().isEmpty()
					|| this.getVistaMU().getTxtLocalidad().getText().isEmpty()
					|| this.getVistaMU().getComboTipoUsuario().getActionCommand().isEmpty()) {

				JOptionPane.showMessageDialog(this.getVistaMU(), "Todos los campos son obligatorios");

			} else {

				int privilegios = resultadoCombBox(
						this.getVistaMU().getComboTipoUsuario().getSelectedItem().toString());

				Usuario us = new Usuario(Integer.valueOf(this.getVistaMU().getTxtDNI().getText()),
						this.getVistaMU().getTxtnombreUsuario().getName(),
						this.getVistaMU().getTxtContrasenia().getText(), this.getVistaMU().getTxtNombre().getText(),
						this.getVistaMU().getTxtApellido().getText(), this.getVistaMU().getTxtEmail().getText(),
						this.getVistaMU().getTxtTelefono().getText(), this.getVistaMU().getTxtDomicilio().getText(),
						this.getVistaMU().getTxtLocalidad().getText(), privilegios);

				Integer rta = JOptionPane.showConfirmDialog(this.getVistaMU(),
						"Esta seguro que desea guardar los cambios?", null, JOptionPane.YES_NO_OPTION);

				if (rta == 0) {
					try {
						int id = this.getUsuario().obteneridUsuario(this.getVistaMU().getTxtDNI().getText());

						System.out.println("contrasenia usuario" + us.getContrasenia());
						Boolean creado = Usuario.getInstance().modificarUsuario(us, id);

						System.out.println("contrasenia" + us.getContrasenia());

						if (creado) {
							JOptionPane.showMessageDialog(getVistaMU(), "Usuario modificado con exito");
						} else {
							JOptionPane.showMessageDialog(getVistaMU(), "El usuario no ha sido modificado");
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block

					}
				}

			}
		} // TODO Auto-generated method stub

	}

	public int resultadoCombBox(String tipoUsuario) {
		int permiso = 0;
		if (tipoUsuario.equals("Administrador")) {
			permiso = 1;

		}
		if (tipoUsuario.equals("Medico")) {
			permiso = 2;
		} else {

			permiso = 3;
		}

		return permiso;
	}

	public Vista13_Modificar_usuario getVistaMU() {
		return vistaMU;
	}

	public void setVistaMU(Vista13_Modificar_usuario vistaMU) {
		this.vistaMU = vistaMU;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		
		System.out.println("DNI usuario vista modificarU: "+getUsuario().getDni().toString());

		this.getVistaMU().getTxtDNI().setText(getUsuario().getDni().toString());
		
		System.out.println("DNI usuario vista modificarUSS: "+this.getVistaMU().getTxtDNI());
		
		this.getVistaMU().getTxtnombreUsuario().setText(getUsuario().getNombreUsuario().toString());
		this.getVistaMU().getTxtContrasenia().setText(getUsuario().getContrasenia().toString());
		this.getVistaMU().getTxtNombre().setText(getUsuario().getNombre().toString());
		this.getVistaMU().getTxtApellido().setText(getUsuario().getApellido().toString());
		this.getVistaMU().getTxtEmail().setText(getUsuario().getEmail().toString());
		this.getVistaMU().getTxtTelefono().setText(getUsuario().getTelefono().toString());
		this.getVistaMU().getTxtDomicilio().setText(getUsuario().getDomicilio().toString());
		this.getVistaMU().getTxtLocalidad().setText(getUsuario().getLocalidad().toString());
		this.getVistaMU().getComboTipoUsuario().setSelectedItem(getUsuario().getTipoUsuario().intValue());

		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public Controlador1_PP getControladorPP() {
		return controladorPP;
	}

	public void setControladorPP(Controlador1_PP controladorPP) {
		this.controladorPP = controladorPP;
	}

}
