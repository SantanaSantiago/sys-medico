package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vista.Vista8_reportes;

public class Controlador8_reportes implements ActionListener {
	
	private Vista8_reportes vistaReportes;

	public Controlador8_reportes() {
		this.setVistaReportes(new Vista8_reportes(this));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public Vista8_reportes getVistaReportes() {
		return vistaReportes;
	}

	public void setVistaReportes(Vista8_reportes vistaReportes) {
		this.vistaReportes = vistaReportes;
	}

}
