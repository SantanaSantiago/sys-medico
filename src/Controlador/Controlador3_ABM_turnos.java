package Controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Vista.Vista1_PP;
import Vista.Vista3_ABM_turnos;

public class Controlador3_ABM_turnos implements ActionListener {

	private Vista3_ABM_turnos vistaTurnos;
	private Controlador1_PP controladorPP;

	/// 3
	public Controlador3_ABM_turnos(Controlador1_PP controladorPP) {
		this.setVistaTurnos(new Vista3_ABM_turnos(this));
		// 4
		this.controladorPP = controladorPP;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton botones = (JButton) e.getSource();
		

		if (botones == this.getVistaTurnos().getBtnCargar()) {
			CardLayout cl = (CardLayout) this.getControladorPP().getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getControladorPP().getVistaPrincipal().getPanel_Card(), "CARGAR_TU");
		}
	}

	public Vista3_ABM_turnos getVistaTurnos() {
		return vistaTurnos;
	}

	public void setVistaTurnos(Vista3_ABM_turnos vistaTurnos) {
		this.vistaTurnos = vistaTurnos;
	}

	public Controlador1_PP getControladorPP() {
		return controladorPP;
	}

	public void setControladorPP(Controlador1_PP controladorPP) {
		this.controladorPP = controladorPP;
	}

}
