package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vista.Vista6_Abm_historial;

public class Controlador6_Abm_historial implements ActionListener {

	private Vista6_Abm_historial vista_historial;
    
	
	
	public Controlador6_Abm_historial() {
		
		this.setVista_historial(new Vista6_Abm_historial(this));
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
	}

	
	public Vista6_Abm_historial getVista_historial() {
		return vista_historial;
	}

	public void setVista_historial(Vista6_Abm_historial vista_historial) {
		this.vista_historial = vista_historial;
	}
	
	
	
}
