package Controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador.CargarPanel;
import Modelo.Usuario;
import Vista.Vista1_PP;
import Vista.Vista4_ABM_usuarios;

public class Controlador4_ABM_usuarios implements ActionListener, WindowListener {

	private Vista4_ABM_usuarios vistaUsuarios;
	private Controlador1_PP controladorPrincipal;
	private Usuario usuario;

	public Controlador4_ABM_usuarios(Controlador1_PP controladorPrincipal) {
		System.out.println("Controlador Usuarios");

		this.setVistaUsuarios(new Vista4_ABM_usuarios(this));
		this.controladorPrincipal = controladorPrincipal;
		this.windowActivated(null);
		this.setUsuario(usuario);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVistaUsuarios().getBtnCargar()) {
			System.out.println("pasa por aca... panel cargar u");

			CardLayout cl = (CardLayout) this.getControladorPrincipal().getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getControladorPrincipal().getVistaPrincipal().getPanel_Card(), "CARGAR_US");

		}

		if (e.getSource() == this.getVistaUsuarios().getBtnEliminar()) {

			int selec = this.getVistaUsuarios().getTablaUsuarios().getSelectedRow();
			if (selec == -1) {

				System.out.println("NO SELECCIONO NADA");
				JOptionPane.showMessageDialog(getVistaUsuarios(), "Seleccione un Usuario de la tabla");
			} else {

				String baja = (String) this.getVistaUsuarios().getTablaUsuarios().getValueAt(selec, 0);
				System.out.println(baja);
				String usuarioBaja = (String) this.getVistaUsuarios().getTablaUsuarios().getValueAt(selec, 0);
				Integer rta = JOptionPane.showConfirmDialog(getVistaUsuarios(),
						"Esta seguro que desea eliminar al ususario: " + usuarioBaja, "Eliminar usuario",
						JOptionPane.YES_NO_OPTION);
				if (rta == 0) {

					try {
						Usuario usuario = new Usuario();
						boolean dadoBaja = usuario.eliminarUsuario(baja);
						if (dadoBaja) {

							JOptionPane.showMessageDialog(getVistaUsuarios(), "El Usuario se ha eliminado! ", null,
									JOptionPane.INFORMATION_MESSAGE, null);

							
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
			this.windowActivated(null);
		}

		// MODIFICO USUARIO
		if (e.getSource() == this.getVistaUsuarios().getBtnModificar()) {


			int selec1 = this.getVistaUsuarios().getTablaUsuarios().getSelectedRow();
			if (selec1 == -1) {

				System.out.println("NO SELECCIONO NADA");
				JOptionPane.showMessageDialog(getVistaUsuarios(), "Seleccione un Usuario de la tabla");
			} else {

				String nombre = (String) this.getVistaUsuarios().getTablaUsuarios().getValueAt(selec1, 0);
				String dni = (String) this.getVistaUsuarios().getTablaUsuarios().getValueAt(selec1, 2);
				System.out.println(dni);
				String privilegios = (String) this.getVistaUsuarios().getTablaUsuarios().getValueAt(selec1, 3);

				System.out.println(dni);

				Usuario usu = new Usuario();
				Integer id = usu.obteneridUsuario(dni);
				System.out.println("ID USUARIO : " + id.toString());

				usu = usu.datosUsuario(id);
				System.out.println(usu.toString());

				System.out.println("modifica");
				Controlador13_Modificar_usuario controladorMU = new Controlador13_Modificar_usuario(usu,controladorPrincipal);
				
				/*
				 * CardLayout cl = (CardLayout)
				 * this.getControladorPrincipal().getVistaPrincipal().getPanel_Card().getLayout(
				 * );
				 * cl.show(this.getControladorPrincipal().getVistaPrincipal().getPanel_Card(),
				 * "MODIFICAR_U");
				 */
			}
		}

	}

	public Vista4_ABM_usuarios getVistaUsuarios() {
		return vistaUsuarios;
	}

	public void setVistaUsuarios(Vista4_ABM_usuarios vistaUsuarios) {
		this.vistaUsuarios = vistaUsuarios;
	}

	public Controlador1_PP getControladorPrincipal() {
		return controladorPrincipal;
	}

	public void setControladorPrincipal(Controlador1_PP controladorPrincipal) {
		this.controladorPrincipal = controladorPrincipal;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

		DefaultTableModel actualizaTabla; // = BaseDeDatos.getInstance().

		try {
			actualizaTabla = Usuario.getInstance()
					.actualizarTablaUsuario(this.getVistaUsuarios().getTxtBusqueda().getText());
			this.getVistaUsuarios().getTablaUsuarios().setModel(actualizaTabla);

		} catch (Exception f) {
		}

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
