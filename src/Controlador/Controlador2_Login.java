package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import com.sun.javafx.embed.swing.Disposer;

import Modelo.Usuario;
import Vista.Vista2_Login;
import Vista.VistaLogin;

public class Controlador2_Login implements ActionListener {

	private VistaLogin vistaLogin;
	
	private Usuario usuario;

	public Controlador2_Login() {
		this.setVistaLogin(new VistaLogin(this));
		this.setUsuario(usuario);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == this.getVistaLogin().getBtnCancelar()) {
			this.getVistaLogin().dispose();
		
		
		}

		if (e.getSource() == this.getVistaLogin().getBtnAceptar()) {

			Usuario usuario = new Usuario();
			System.out.println(usuario.getNombreUsuario());
			System.out.println(usuario.getContrasenia());
			
			if(usuario.login(this.getVistaLogin().getTxtNombreUsuario().getText(),this.getVistaLogin().getTxtPassword().getText())) {
				
			
		
		
		Integer tipoU = usuario.obtenerTipoUsuario(this.getVistaLogin().getTxtNombreUsuario().getText());
		
		Controlador1_PP controladorPP = new Controlador1_PP();
		controladorPP.vistaTipoUsuario(tipoU);
		
		}
		this.getVistaLogin().dispose();
		}

	}

	public VistaLogin getVistaLogin() {
		return vistaLogin;
	}

	public void setVistaLogin(VistaLogin vistaLogin) {
		this.vistaLogin = vistaLogin;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
